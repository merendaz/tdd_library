
public class LibraryApp {
	private boolean isLoggedIn = false;
	// Empty constructor
	public LibraryApp() {
		
	}
	public boolean adminLoggedIn() {
		// Return a property that indicates if the admin is logged in or
		// logged out
		return this.isLoggedIn;
	}
	public boolean adminLogin(String password) {
	// Function assumes:
	// 	1. There is only 1 administrator
	//  2. Admin's password = "adminadmin"
		if (password.equals("adminadmin")) {
			this.isLoggedIn = true;
		}
		else {
			this.isLoggedIn = false;
		}
		return this.isLoggedIn;
	}
}
